const path = require('path')

const RemoteShellScript = require('../lib/RemoteShellScript')

const testFilePath = `${path.join(__dirname, 'test.sh')}`

describe('Initialization Tests', () => {

    test('Should be defined and have the proper class', () => {
        expect(RemoteShellScript).toBeDefined()
        expect(RemoteShellScript).toBeInstanceOf(Function)
    })
    
    test('Instantiates correctly when passed options', () => {
        const script = new RemoteShellScript({ 
            path: testFilePath, 
            args: [ 'one' ], 
            host: {
                host: '127.0.0.1',
                port: 22,
                username: 'user',
                password: 'password',
                privateKey: '/path/to/key'
            } 
        })
        expect(script.path).toBe(testFilePath)
        expect(script.args).toEqual([ 'one' ])
        expect(script.host).toEqual({
            host: '127.0.0.1',
            port: 22,
            username: 'user',
            password: 'password',
            privateKey: '/path/to/key'
        })
    })
    
    test('Throws error if not passed a path', () => {
        expect(() => new RemoteShellScript({ host: {} })).toThrow('path is required')
    })

    test('Throws error if not given proper host config', () => {
        expect(() => new RemoteShellScript({ path: testFilePath })).toThrow('host parameters are required')
        expect(() => new RemoteShellScript({ path: testFilePath, host: {} })).toThrow('host address is required')
    })

    test('RemoteShellScript.run returns a Promise', () => {
        const script = new RemoteShellScript({ 
            path: testFilePath, 
            host: { host: '127.0.0.1' } 
        })
        expect(script.run()).toBeInstanceOf(Promise)
    })
})


describe('Usage Tests', () => {

    const scriptAsPromise = config => {
        return new Promise((resolve, reject) => {
            const script = new RemoteShellScript({
                ...config,
                done: code => {
                    if (config.done) config.done(code)
                    resolve()
                },
            })
            script.run()
        })
    }

    // TODO:  Have to mock out ssh2 to finish this

//     test('Exposes a ChildProcess for the script', () => {
//         const script = new RemoteShellScript({ path: testFilePath })
//         script.run()
//         expect(script.process).toBeInstanceOf(ChildProcess)
//     })

//     test('Script runs callback with proper code when done', async () => {
//         const done = jest.fn()
//         await scriptAsPromise({
//             path: testFilePath,
//             args: ['error'],
//             done,
//         })
//         expect(done).toHaveBeenCalledWith(1)
//     })

//     test('Script pipes stdout correctly', async () => {
//         const stdout = jest.fn()
//         await scriptAsPromise({
//             path: testFilePath,
//             stdout,
//         })
//         expect(stdout).toHaveBeenCalledWith(Buffer.from('This is output\n'))
//     })

//     test('Script pipes stderr correctly', async () => {
//         const stderr = jest.fn()
//         await scriptAsPromise({
//             path: testFilePath,
//             args: [ 'error' ],
//             stderr,
//         })
//         expect(stderr).toHaveBeenCalledWith(Buffer.from('WARN: This is a warning\n'))
//         expect(stderr).toHaveBeenCalledWith(Buffer.from('ERROR: This is an error\n'))
//     })
})
