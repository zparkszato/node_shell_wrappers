#!/bin/bash

throw() {
    cat <<< "ERROR: $@" 1>&2
    exit 1
}
warn() { cat <<< "WARN: $@" 1>&2; }

export -f throw
export -f warn

echo "This is server output"

sleep 0.1s

echo "This is more output"

sleep 0.1s

echo "This is more output"

sleep 0.1s

warn "This is a server warning"

if [[ $1 == "error" ]]; then
    throw "This is a server error"
fi