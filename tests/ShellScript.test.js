const { ChildProcess } = require('child_process')
const path = require('path')
const delay = require('delay')

const ShellScript = require('../lib/ShellScript')

const testFilePath = `${path.join(__dirname, 'test.sh')}`
const testCwdFilePath = `${path.join(__dirname, 'testCwd.sh')}`

describe('Initialization Tests', () => {

    test('Should be defined and have the proper class', () => {
        expect(ShellScript).toBeDefined()
        expect(ShellScript).toBeInstanceOf(Function)
    })
    
    test('Instantiates correctly when passed options', () => {
        const script = new ShellScript({ path: testFilePath, args: [ 'one' ]})
        expect(script.path).toBe(testFilePath)
        expect(script.args).toEqual([ 'one' ])
    })
    
    test('Throws error if not passed a path', () => {
        expect(() => new ShellScript({})).toThrow('path is required')
    })
})


describe('Usage Tests', () => {

    const scriptAsPromise = async config => {
        const script = new ShellScript(config)
        return await script.run()
        // return new Promise((resolve, reject) => {
        //     const script = new ShellScript({
        //         ...config,
        //         done: code => {
        //             if (config.done) config.done(code)
        //             resolve()
        //         },
        //     })
        //     script.run()
        // })
    }

    test('Exposes a ChildProcess for the script', () => {
        const script = new ShellScript({ path: testFilePath })
        script.run()
        expect(script.process).toBeInstanceOf(ChildProcess)
    })

    test('Script runs callback with proper code when done', async () => {
        const done = jest.fn()
        await scriptAsPromise({
            path: testFilePath,
            args: ['error'],
            done,
        })
        expect(done).toHaveBeenCalledWith(1, null)
    })

    test('Script pipes stdout correctly', async () => {
        const stdout = jest.fn()
        await scriptAsPromise({
            path: testFilePath,
            stdout,
        })
        expect(stdout).toHaveBeenCalledWith(Buffer.from('This is server output\n'))
    })

    test('Script pipes stderr correctly', async () => {
        const stderr = jest.fn()
        await scriptAsPromise({
            path: testFilePath,
            args: [ 'error' ],
            stderr,
        })
        expect(stderr).toHaveBeenCalledWith(Buffer.from('WARN: This is a server warning\n'))
        expect(stderr).toHaveBeenCalledWith(Buffer.from('ERROR: This is a server error\n'))
    })

    test('Script uses CWD correctly', async () => {
        const stdout = jest.fn()
        await scriptAsPromise({
            path: testCwdFilePath,
            cwd: __dirname,
            stdout,
            // stdout: data => console.log({ data: data.toString(), current: __dirname }),
        })
        expect(stdout).toHaveBeenCalledWith(Buffer.from(`${__dirname}\n`))
    })

})

describe('Script cancel tests', () => {
    const scriptPromise = (sig, done) => new Promise((resolve, reject) => {
        const script = new ShellScript({
            path: testFilePath,
            done: (code, signal) => {
                done(code, signal)
                resolve()
            },
        })
        let cancelFn = () => script.kill()
        if (sig === 'SIGINT') cancelFn = () => script.interrupt()
        if (sig === 'SIGTERM') cancelFn = () => script.terminate()
        script.run()
        setTimeout(cancelFn, 0)
    })

    test('Script interrupts correctly', async () => {
        const done = jest.fn()
        await scriptPromise('SIGINT', done)
        expect(done).toHaveBeenCalledWith(null, 'SIGINT')
    })
    test('Script terminates correctly', async () => {
        const done = jest.fn()
        await scriptPromise('SIGTERM', done)
        expect(done).toHaveBeenCalledWith(null, 'SIGTERM')
    })
    test('Script kills correctly', async () => {
        const done = jest.fn()
        await scriptPromise('SIGKILL', done)
        expect(done).toHaveBeenCalledWith(null, 'SIGKILL')
    })
})
