const os = require('os')
const path = require('path')
const fs = require('fs')
const delay = require('delay')

const RemoteShellScript = require('../lib/RemoteShellScript')

async function main() {
    const script = new RemoteShellScript({
        path: path.join(__dirname, '/testLongRun.sh'),
        // args: [ 'error' ],
        stdout: data => console.log(`STDOUT - ${data}`),
        stderr: data => console.log(`STDERR - ${data}`),
        done: (code, signal) => console.log(`Process exited with code ${code} and signal ${signal}`),
        onConnect: () => console.log('Connection established'),
        onDisconnect: () => console.log('Connection closed'),
        onConnectError: err => console.log(`Connection error: ${err.message}`),
        host: {
            host: '127.0.0.1',
            username: os.userInfo().username,
            privateKey: fs.readFileSync(`${os.userInfo().homedir}/.ssh/id_rsa`)
        },
    })
    try {
        script.run()
        await delay(3000)
        script.terminate()
        await delay(1000)
    } catch(err) {
        
    }
    
}

main()