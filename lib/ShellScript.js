const path = require('path')
const { spawn } = require('child_process')

class ShellScript {
    constructor(options) {
        if (!options.path) throw 'path is required'
        this.cwd = options.cwd || path.dirname(options.path)
        this.path = options.path
        this.args = options.args || []
        this.command = options.command || 'bash'
        this.done = options.done ? options.done : () => {}
        this.stdout = options.stdout ? options.stdout : () => {}
        this.stderr = options.stderr ? options.stderr : () => {}
    }

    run() {
        return new Promise((resolve, reject) => {
            this.process = spawn(this.command, [ this.path, ...this.args ], { cwd: this.cwd })
            this.process.stdout.on('data', this.stdout)
            this.process.stderr.on('data', this.stderr)
            this.process.on('close', (code, signal) => {
                this.done(code, signal)
                resolve()
            })
        })
    }

    interrupt() {
        this.process.kill('SIGINT')
    }

    terminate() {
        this.process.kill('SIGTERM')
    }

    kill() {
        this.process.kill('SIGKILL')
    }
}

module.exports = ShellScript