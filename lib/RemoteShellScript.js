const { Client } = require('ssh2')

const ShellScript = require('./ShellScript')

class RemoteShellScript extends ShellScript {
    constructor(options) {
        super(options)
        if (!options.host) throw "host parameters are required"
        if (!options.host.host) throw "host address is required"
        this.client = new Client()
        this.host = {
            ...options.host
            // host: options.host.host,
            // port: options.host.port || 22,
            // username: options.host.username,
            // password: options.host.password,
            // privateKey: options.host.privateKey ? fs.readFileSync(options.host.privateKey).toString() : null
        }
        this.onConnect = options.onConnect ? options.onConnect : () => {}
        this.onDisconnect = options.onDisconnect ? options.onDisconnect : () => {}
        this.onConnectError = options.onConnectError ? options.onConnectError : () => {}
        this.stream = null
        this.pid = null
    }

    run() {
        const command = `${this.command} ${this.path} ${this.args.join(' ')}`
        return new Promise((resolve, reject) => {
            this.client
            .on('ready', () => {
                this.onConnect()
                this.client.exec(`echo "EXEC PID: $$";cd ${this.cwd} && ${command}`, { pty: true }, (err, stream) => {
                    if (err) throw err
                    this.stream = stream
                    stream
                    .on('close', (code, signal) => {
                        this.done(code, signal)
                        this.stream = null
                        this.client.end()
                        resolve(code)
                    })
                    .on('data', data => {
                        const line = data.toString()
                        if (line.substr(0, 10) === 'EXEC PID: ') {
                            this.pid = line.substr(10).trim()
                        } else {
                            this.stdout(data)
                        }
                    })
                    .stderr.on('data', this.stderr)
                })
            })
            .on('error', err => {
                console.error(err)
                this.onConnectError(err)
                reject(err)
            })
            .on('end', () => {
                this.pid = null
                this.onDisconnect(arguments)
            })
            .connect(this.host)
        })
    }

    interrupt() {
        // if (this.stream) this.stream.signal('INT')
        if (this.pid) this.client.exec(`kill -2 ${this.pid}`, () => {})
    }
    terminate() {
        // if (this.stream) this.stream.signal('TERM')
        if (this.pid) this.client.exec(`kill -15 ${this.pid}`, () => {})
    }
    kill() {
        if (this.pid) this.client.exec(`kill -9 ${this.pid}`, () => {})
        // if (this.stream) this.stream.signal('KILL')
    }
}

module.exports = RemoteShellScript