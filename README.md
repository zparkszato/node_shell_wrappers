# Node Shell Wrappers

Written by Zach Parks, Senior Software Engineer



This module exposes 2 classes that allow for easier running of bash scripts using Node.js.  Each of them exposes a common interface, with one allowing for local script execution (using Node's built-in **child_process** module) and the other allowing for remote script execution (using the **ssh2** npm module).



These two classes are **ShellScript** and **RemoteShellScript**, for local and remote running respectively.  They take exactly the same config, but SSH creds need to be passed when running remote scripts, and some extra event hooks are provided.



### API



Both classes take a singe options object as input, with the following possible options:



- **path:** (required)  The path to the file to run.  Recommend using absolute paths.
- **args:** Array containing any command line args to pass to the file.
- **stdout:** Function that will be called every time the file pipes output to stdout.  Takes the data sent as its only parameter.  Note that this is sent without processing, and is usually a Node Buffer.  It's up to the module user to convert it to a string if they prefer.
- **stderr:** Function that will be called every time the file pipes output to stderr.  Exactly the same as stdout otherwise.
- **done:** Function that will be called when the process terminates.  Takes exit code and signal as parameters.



**RemoteShellScript** additionally takes the following options:



- **onConnect:** Function that will be called when the SSH connection to the target server is successfully established.  Takes no arguments.
- **onDisconnect:** Function that will be called when the SSH connection ends.  Takes no arguments.
- **onConnectError:** Function that will be called if there is an error connecting.  Takes the error as an argument
- **host:** (required) Object containing connection config.  Has the following options:
  - **host.host:** (required) The host address
  - **host.port:** The port (22 by default)
  - **host.username:** The username to use to connect
  - **host.password:** Password for that user, if applicable
  - **host.privateKey:** Path to the private key file to use, if applicable



### Sample Usage



Once the script object is initiated, you must use the **run** method to actually run the script.  Note that the **run** method returns a promise that resolves when the script terminates.





#### ShellScript

```javascript
const ShellScript = require('node_shell_wrappers').ShellScript

// Set up the Script
const script = new ShellScript({
    path: '/home/user/test.sh',
    args: [ 'month', 'year' ],
    stdout: data => console.log(`STDOUT - ${data}`),
    stderr: data => console.log(`STDERR - ${data}`),
    done: (code, signal) => console.log(`Process exited with code ${code} and signal ${signal}`)
})

// Run the script
script.run()
```



#### RemoteShellScript

```javascript
const RemoteShellScript = require('node_shell_wrappers').RemoteShellScript

// Set up the Script
const script = new RemoteShellScript({
    path: '/home/user/test.sh',
    args: [ 'month', 'year' ],
    stdout: data => console.log(`STDOUT - ${data}`),
    stderr: data => console.log(`STDERR - ${data}`),
    done: (code, signal) => console.log(`Process exited with code ${code} and signal ${signal}`),
    onConnect: () => console.log('Connection established'),
    onDisconnect: () => console.log('Connection closed'),
    onConnectError: err => console.log(`Connection error: ${err.message}`),
    host: {
        host: 'your.path.here',
        username: 'user',
        password: 'password',
    }
})

// Run the script
script.run()
```

